// TODO: min 29
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Pagination from 'react-js-pagination';

import {TituloEncabezado} from './components/titulo';
import {Buscador} from './components/buscador';
import {EmojiList} from './components/emojiList';
import {useState, useEffect} from 'react';
//import baseDatos from './database/emoji-list.json'; //baseDatos.emojis


let LIMIT = 24;
//http://localhost:3001/emojis?_limit=25&_page=1
let URL = `http://localhost:3001/emojis?_limit=${LIMIT}`; 


function App() {

  let [emojis, setEmojis] = useState([]);
  let [busqueda, setBusqueda] = useState('');
  let [pagina, setPagina] = useState(1);

  let emojisFilters = emojis.filter((emoji) => {
    let emojiTitleLower = emoji.title.toLowerCase();
    let busquedaLower = busqueda.toLowerCase();
    
    if (emojiTitleLower.includes(busquedaLower)) {
      return emoji;
    }
  });
  
  useEffect( () => {
    fetch(`${URL}&_page=${pagina}`)
      .then(res => res.json())
      .then(datos => {
        setEmojis(datos);
      })
  }, [pagina]);

  // async function obtenerDatos() {
  //   let respuesta = await fetch(URL);
  //   let datos = await respuesta.json();
  //   setEmojis(datos);
  // }
  // obtenerDatos();

  function actualizarInput(evento) {
    let entradaTeclado = evento.target.value;
    setBusqueda(entradaTeclado);
  }

  function handlePageChange(nuevaPagina) {
    setPagina(nuevaPagina);
  }

  return (
    <div className="App container">
      <TituloEncabezado/>
      <Buscador valorInput={busqueda} onInputChange={actualizarInput}/>
      <EmojiList datos={busqueda ? emojisFilters : emojis}/>

      <div className="d-flex justify-content-center">
        <Pagination
          itemClass="page-item"
          linkClass="page-link"
          activePage={pagina}
          itemsCountPerPage={LIMIT}
          totalItemsCount={1820}
          pageRangeDisplayed={7}
          onChange={handlePageChange}
        />
      </div>
    </div>
  )
}

export default App
