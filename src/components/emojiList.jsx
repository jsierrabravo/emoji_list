import {EmojiItem} from './emojiItem'
export function EmojiList({datos}){
    
    let renderEmojis = datos.map((emoji) => { //datos.slice(0,50)
        return (
          <div className="col-3 my-2" key={emoji.title}>
            <EmojiItem title={emoji.title} keywords={emoji.keywords} symbol={emoji.symbol}/>
          </div>
        );
      })

    return (
        <div className="row py-5">
            {renderEmojis}
        </div>
    );
}