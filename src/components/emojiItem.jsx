export function EmojiItem({title, symbol, keywords}) {
    return (
        <div className="card text-center">
            <h5 className="card-header">{title}</h5>
            <div className="card-body">
                <h1>{symbol}</h1>
                <p className="card-text">{keywords}</p>
            </div>
        </div>
    );
}