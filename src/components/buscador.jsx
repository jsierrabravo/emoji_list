export function Buscador({valorInput, onInputChange}) {
    return (
        <div className="row">
            <div className="col-6"></div>
            <div className="mb-3 col-6">
                <input 
                    value={valorInput}
                    onChange={onInputChange}
                    type="text" 
                    className="form-control" 
                    placeholder="Buscar emoji"
                />
            </div>
        </div>
    );
}